package es.talent.patrone.adapter2;

public class AdaptadorMicroSDaSD implements SD {

	private SamsungMicroSD microSD;
	
	public String transferirInformacion() {
		return this.microSD.transfer();
	}
	
	public void insertarMicroSDEnAdaptador(SamsungMicroSD micro) {
		this.microSD = micro;
	}
	

}
