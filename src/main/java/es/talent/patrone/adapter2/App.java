package es.talent.patrone.adapter2;

public class App {

	public static void main(String[] args) {
		Ordenador ord = new Ordenador();
		SD tarjetaSamsung = new SamsungSD();
		SamsungMicroSD microSD = new SamsungMicroSD();
		AdaptadorMicroSDaSD adaptador = new AdaptadorMicroSDaSD();
		adaptador.insertarMicroSDEnAdaptador(microSD);
		
		ord.insertarTarjetaEnRanura(tarjetaSamsung);
		ord.leerDatosSD();
		
		ord.insertarTarjetaEnRanura(adaptador);
		ord.leerDatosSD();
	}

}
