package es.talent.patrones.adapter;

public class App {

	public static void main(String[] args) {
		Reproductor rep = new Reproductor();
		VHS cinta = new CintaVHS();
		MiDV cintaMiDV = new CintaMiDV();
		AdaptadorMiDVaVHS adaptador = new AdaptadorMiDVaVHS();
		
		rep.introducirCinta(cinta);
		rep.reporducirCinta();
		
		adaptador.insertarMiDVenAdaptador(cintaMiDV);
		rep.introducirCinta(adaptador);
		rep.reporducirCinta();
	}

}
