package es.talent.patrones.adapter;

public interface MiDV {
	public String muestraFrame1();

	public String muestraFrame2();

	public String muestraFrame3();

	public String muestraFrame4();

	public String muestraFrame5();
}
