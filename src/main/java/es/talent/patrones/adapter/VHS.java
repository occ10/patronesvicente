package es.talent.patrones.adapter;

public interface VHS {
	public String siguienteFrame();
	public boolean fin();
}
