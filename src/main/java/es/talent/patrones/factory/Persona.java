package es.talent.patrones.factory;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public abstract class Persona {
	protected String nombre;
	protected String apellidos;
	protected int edad;

}
