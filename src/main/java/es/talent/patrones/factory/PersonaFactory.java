package es.talent.patrones.factory;

public class PersonaFactory {
	public static Persona crearPersona(String tipo) {
		Persona nuevaPersona = null;
		if ("P".equals(tipo)) {
			nuevaPersona = new Profesor();
		} else if ("A".equals(tipo)) {
			nuevaPersona = new Alumno();
		}
		return nuevaPersona;
	}
}
